/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_lab3;

import java.io.IOException;
import java.io.FileWriter;
import java.io.Writer;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Title: Mini Project 3 - XML
 * @author Dion
 * Date:  03/03/2017
 */
public class Stax {
    
    /**
     * testing method
     *
     * @param args
     */
    public static void main(String[] args) {
        Stax.StaxCreate("a", "b", "c", "d");
    }

    /**
     * Creates the xml by using parameters
     * @param dateRan
     * @param term
     * @param found
     * @param geoNameID
     */
    public static void StaxCreate(String dateRan, String term, String found, String geoNameID) {
        try {
            String filePath = "D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\stax.xml";
            Writer fileWriter = new FileWriter(filePath);
            
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = factory.createXMLStreamWriter(fileWriter);
            
            writer.writeStartDocument();
            writer.writeCharacters("\n");
            writer.writeStartElement("weatherSearches");
            writer.writeCharacters("\n\t");
            writer.writeStartElement("search");
            writer.writeAttribute("date", dateRan);
            writer.writeCharacters("\n\t\t");
            writer.writeStartElement("term");
            writer.writeCharacters(term);
            writer.writeEndElement();
            writer.writeCharacters("\n\t\t");
            writer.writeStartElement("found");
            writer.writeCharacters(found);
            writer.writeEndElement();
            writer.writeCharacters("\n\t\t");
            writer.writeStartElement("geoNameID");
            writer.writeCharacters(geoNameID);
            writer.writeEndElement();
            writer.writeCharacters("\n\t");
            writer.writeEndElement();
            writer.writeCharacters("\n");
            writer.writeEndElement();
            writer.writeEndDocument();

            writer.flush();
            writer.close();

        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }
}
