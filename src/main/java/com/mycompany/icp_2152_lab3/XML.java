/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_lab3;

//import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * Title: Mini Project 3 - XML
 * @author Dion
 * Date:  03/03/2017
 */
public class XML {

    private static final String urlString = "http://open.live.bbc.co.uk/weather/feeds/en/ll74/observations.rss";
    String forecast;
    String number1;

    /**
     * geoNameID value for reference
     */
    public static String geoNameID;

    /**
     *
     * @param url
     * @param element1
     * @param element2
     * @return
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public String getElementData(String url, String element1, String element2) throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = f.newDocumentBuilder();
        Document doc = db.parse(url);
        NodeList items = doc.getElementsByTagName(element1);

        for (int i = 0; i < items.getLength(); i++) {
            Node n = items.item(i);
            if (n.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            Element e = (Element) n;
            NodeList descList = e.getElementsByTagName(element2);
            Element descElem = (Element) descList.item(0);
            // get the "text node" in the title (only one)
            Node descNode = descElem.getChildNodes().item(0);
            //System.out.println(titleNode.getNodeValue());
            forecast = descNode.getNodeValue();
        }
        return forecast;
    }

    /**
     *
     * @param url
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public String getLocation(String url) throws ParserConfigurationException, SAXException, IOException {

        String returnURL = "";
        String number = "";
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = f.newDocumentBuilder();
        Document doc = db.parse(url);
        //   Document doc = db.parse(urlString);
        // get the "title elem" in this item (only one)
        NodeList items = doc.getElementsByTagName("geoname");

        for (int i = 0; i < items.getLength(); i++) {
            Node n = items.item(i);
            if (n.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            Element e = (Element) n;
            NodeList titleList = e.getElementsByTagName("geonameId");
            Element titleElem = (Element) titleList.item(0);
            // get the "text node" in the title (only one)
            Node titleNode = titleElem.getChildNodes().item(0);
            //System.out.println(titleNode.getNodeValue());
            number = titleNode.getNodeValue();
            returnURL = "http://open.live.bbc.co.uk/weather/feeds/en/" + number + "/observations.rss";
            geoNameID = number;
        }
        return returnURL; //returns the ammended url
    }

    /**
     *
     * @param getUrl
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public String getWeatherIcon(String getUrl) throws MalformedURLException, IOException {

        String path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\error.png");
        URL url = new URL(getUrl);
        InputStream stream = url.openStream();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                if (sCurrentLine.contains("Sunny")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\sunny.png");
                } else if (sCurrentLine.contains("Cloudy") || sCurrentLine.contains("Thick Cloud")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\cloudy.png");
                } else if (sCurrentLine.contains("Light Cloud")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\light_clouds.png");
                } else if (sCurrentLine.contains("Windy")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\windy.png");
                } else if (sCurrentLine.contains("Heavy rain")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\heavy_rain.png");
                } else if (sCurrentLine.contains("Light Rain") || sCurrentLine.contains("Rain Showers")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\light_showers.png");
                } else if (sCurrentLine.contains("Snow")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\snow.png");
                } else if (sCurrentLine.contains("Lightning")) {
                    path = ("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\ICP_2152_Lab3\\weatherImg\\lightning.png");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    /**
     *
     * @param getUrl
     * @return
     * @throws MalformedURLException
     * @throws IOException
     * opens and reads xml url
     */
    public String readXML(String getUrl) throws MalformedURLException, IOException {
        URL url = new URL(getUrl);
        InputStream stream = url.openStream();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getUrl;
    }

    /**
     * Splits the string up into items and places into a list
     * @param data
     * @return itemList
     */
    public List<String> Split(String data) {

        String[] items = data.split(",");
        List<String> itemList = new ArrayList<>();

        for (String item : items) {
            itemList.add(item);
        }

        //combine strings to one
        itemList.add(",");
        String c = itemList.get(7);
        String j = itemList.get(5);
        c = c.concat(j);
        itemList.set(5, c);
        itemList.remove(7);

        //replace amended string and join to existing
        String i = itemList.get(4);
        j = itemList.get(5);
        i = i.concat(j);
        itemList.set(4, i);
        itemList.remove(5);
        return itemList;
    }

    /**
     *
     * @param args
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        String test = "http://open.live.bbc.co.uk/weather/feeds/en/ll74/observations.rss";
        String geo = "http://api.geonames.org/search?q=london&maxRows=1&lang=en&username=eeu6c0";
        XML xml = new XML();

        System.out.println(xml.getWeatherIcon(test));
        System.out.println(xml.getLocation(geo));
        //Read the xml document
        System.out.println(xml.readXML(test));
        //Get the xml description
        System.out.println(xml.getElementData(test, "item", "description"));
        System.out.println(xml.getElementData(test, "item", "title"));
        String desc = xml.getElementData(test, "item", "description");
        System.out.println(xml.Split(desc));
    }
}